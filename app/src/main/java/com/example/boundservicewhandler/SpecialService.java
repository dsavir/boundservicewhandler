package com.example.boundservicewhandler;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;

/**
 * A service that performs a task every 10 seconds using a Handler.
 */
public class SpecialService extends Service {

    boolean isSpecialMode = false;//are we in special mode?

    Handler handler = new Handler(Looper.getMainLooper());//the handler to perform actions

    private final IBinder mBinder = new LocalBinder();//for binding service

    public SpecialService() {
        //default empty constructor
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    //simple binder
    public class LocalBinder extends Binder {
        public SpecialService getService() {
            return SpecialService.this;
        }
    }

    /**
     * start mode that performs task every 10 seconds
     */
    public void startSpecialMode(){
        if (!isSpecialMode){
            handler.post(task);
            isSpecialMode = true;
        }
    }

    /**
     * Stops mode that performs task every 10 seconds and exits
     */
    public void stopSpecialMode(){
        //stop task on handler.
        isSpecialMode = false;
        handler.removeCallbacksAndMessages(null);
        stopSelf();
    }

    /**
     * Task to perform every 10 seconds
     */
    Runnable task = new Runnable() {
        @Override
        public void run() {
            Log.d("Service","running task");
            //perform your task here
            handler.postDelayed(this,10000);//run again in 10 seconds
        }
    };

}