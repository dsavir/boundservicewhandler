package com.example.boundservicewhandler;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.os.PersistableBundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;

import java.util.prefs.PreferenceChangeEvent;

public class MainActivity extends AppCompatActivity {

    boolean isSpecialMode;//are we currently in special mode?
    String mode_key = "special_mode";//key for shared preferences
    SpecialService boundService; //our service
    boolean isBound = false; //is the service bound?
    Button mode; //user button

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //initialize button (can use view binding instead)
        mode = findViewById(R.id.mode_button);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //get isSpecialMOde
        isSpecialMode = PreferenceManager.getDefaultSharedPreferences(this).getBoolean(mode_key,false);
        //if ispecial mode, bind to service
        if (isSpecialMode){
            Intent boundServiceIntent = new Intent(getApplicationContext(),SpecialService.class);
            getApplicationContext().bindService(boundServiceIntent, serviceConnection, BIND_AUTO_CREATE);
            isBound = true;
            mode.setText("Stop Mode");
        }
    }

    //start special mode that performs a task every 10 seconds
    public void startMode(View view){
        if (!isSpecialMode) {//if not already in sepcial mode, start special mode
            isSpecialMode = true;
            PreferenceManager.getDefaultSharedPreferences(this).edit().putBoolean(mode_key, isSpecialMode).apply();
            Intent boundServiceIntent = new Intent(getApplicationContext(), SpecialService.class);
            startService(boundServiceIntent);
            getApplicationContext().bindService(boundServiceIntent, serviceConnection, BIND_AUTO_CREATE);
            isBound = true;
           mode.setText("Stop Mode");
        }
        else { //stop sepecial mode
            mode.setText("Start Mode");
            stopMode();
        }

    }

    //stops special mode and unbinds service
    void stopMode(){
        isSpecialMode = false;
        PreferenceManager.getDefaultSharedPreferences(this).edit().putBoolean(mode_key,isSpecialMode).apply();
        boundService.stopSpecialMode();
        getApplicationContext().unbindService(serviceConnection);
        isBound = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (isBound) {//only unbind if isBound
            getApplicationContext().unbindService(serviceConnection);
        }
    }


    //Simple Service connection for bound service. Start special mode on bind.
    private final ServiceConnection serviceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            boundService = ((SpecialService.LocalBinder) service).getService();
            boundService.startSpecialMode();

        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            boundService = null;
        }
    };
}