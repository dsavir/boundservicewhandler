Android Application demonstrating the use of a bound service with a Handler to perform an action every 10 seconds.

Note that for most use cases, an Alarm would work just as well. This is necessary for tasks that cannot be initialized and closed every 10 seconds, e.g. Bluetooth tasks.

Full explanations in my Medium article [here](https://dsavir-h.medium.com/bound-services-and-handlers-in-android-5a60cb4398f7).
